import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const metrics = {
    state: () => ({
        all: []
    }),
    mutations: {
        setMetric(state, metric) {
            state.all = metric
        }
    },
    actions: {
        async getAllMetrics({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/metrics`, config);
                if (res.status != 200) return;
                commit('setMetric', res.data);
                return;
            } catch (error) {
                return false;
            }
        },
        async addMetric({ commit, state }, metric) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/metrics`, metric, config);
            if (res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editMetric({ commit, state }, metric) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/metrics/${metric._id}`, metric, config);
            if (res.status != 200) return res;
            const idx = state.all.findIndex(c => c._id === metric._id);
            state.all.splice(idx, 1, res.data)
            return res;
        },
        async deleteMetric({ commit, state }, metric) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/metrics/${metric._id}`, config);
            if (res.status != 204) return false;
            const idx = state.all.findIndex(c => c._id === metric._id);
            state.all.splice(idx, 1)
            return true;
        },
    },
}

export default metrics;