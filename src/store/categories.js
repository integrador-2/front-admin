import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const categories = {
    state: () => ({
        all: []
    }),
    mutations: {
        setCategories(state, categories) {
            state.all = categories
        }
    },
    actions: {
        async getAllCategories({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/categories`, config);
                if (res.status != 200) return;
                commit('setCategories', res.data);
                return;
            } catch (error) {
                return false;
            }
        },
        async addCategory({ commit, state }, category) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/categories`, category, config);
            if (res.status != 200 && res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editCategory({ commit, state }, category) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/categories/${category.categoryId}`, category.formCategory, config);
            if (res.status != 200 && res.status != 201) return res;
            const idx = state.all.findIndex(c => c._id === category.categoryId);
            state.all.splice(idx, 1, res.data)
            return res;
        },
        async deleteCategory({ commit, state }, category) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/categories/${category._id}`, config);
            if (res.status != 200 && res.status != 204) return false;
            const idx = state.all.findIndex(c => c._id === category._id);
            state.all.splice(idx, 1);
            return true;
        },
    },
}

export default categories;