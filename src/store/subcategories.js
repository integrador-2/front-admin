import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const subCategories = {
    state: () => ({
        all: []
    }),
    mutations: {
        setSubCategories(state, subCategories) {
            state.all = subCategories
        }
    },
    actions: {
        async getAllSubCategories({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/subcategories`, config);
                if (res.status != 200) return;
                commit('setSubCategories', res.data);
                return;
            } catch (error) {
                return false;
            }
        },
        async addSubCategory({ commit, state }, subCategory) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/subcategories`, subCategory, config);
            if (res.status != 200 && res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editSubCategory({ commit, state }, subCategory) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/subcategories/${subCategory.subCategoryId}`, subCategory.obj, config);
            if (res.status != 200 && res.status != 201) return res;;
            const idx = state.all.findIndex(s => s._id === subCategory.subCategoryId);
            state.all.splice(idx, 1, res.data);
            return res;
        },
        async deleteSubCategory({ commit, state }, subCategory) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/subcategories/${subCategory._id}`, config);
            if (res.status != 200) return;
            const idx = state.all.findIndex(s => s._id === subCategory._id);
            state.all.splice(idx, 1);
            return res;
        },
    },
}

export default subCategories;