import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const status = {
    state: () => ({
        status: []
    }),
    mutations: {
        setStatus(state, list) {
            state.status = list
        }
    },
    actions: {
        async getStatus({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const status = (await axios.get(`${this.state.baseURL}/api/status`, config)).data;
                commit('setStatus', status);
            } catch (error) {
                return false;
            }
        }
    },
    modules: {}
}

export default status;