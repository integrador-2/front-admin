import configHeaderToken from "../libs/configHeaderToken";
import axios from "axios";

const departments = {
  state: () => ({
    all: [],
  }),
  mutations: {
    featuredEntrepreneur(state, featuredEntrepreneur) {
      state.all = featuredEntrepreneur;
    },
  },
  actions: {
    async getFeaturedEntrepreneurs({ commit, state }) {
      try {
        const config = configHeaderToken(this.state.authToken);
        const res = await axios.get(
          `${this.state.baseURL}/api/featured_entrepreneur`,
          config
        );
        if (res.status != 200) return;
        commit("featuredEntrepreneur", res.data);
        return;
      } catch (error) {
        return error;
      }
    },
    async addFeaturedEntrepreneurs({ commit, state }, featured_entrepreneurs) {
      try {
        const config = configHeaderToken(this.state.authToken);
        const res = await axios.post(
          `${this.state.baseURL}/api/featured_entrepreneur`,
          featured_entrepreneurs,
          config
        );
        if (res.status != 200 && res.status != 201) return res;
        state.all.push(res.data);
        return res;
      } catch (error) {
        return error;
      }
    },
    async deleteFeaturedEntrepreneur(
      { commit, state },
      featured_entrepreneur_id
    ) {
      const config = configHeaderToken(this.state.authToken);
      const res = await axios.delete(
        `${this.state.baseURL}/api/featured_entrepreneur/${featured_entrepreneur_id}`,
        config
      );
      if (res.status != 200 && res.status != 201) return res;
      const idx = state.all.findIndex(
        (s) => s._id === featured_entrepreneur_id
      );
      state.all.splice(idx, 1);
      return res;
    },
  },
};

export default departments;
