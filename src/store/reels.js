import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const reels = {
    state: () => ({
        all: [],
        titlePositions: [{
                text: 'Izquierda',
                value: -1
            },
            {
                text: 'Centro',
                value: 0
            },
            {
                text: 'Derecha',
                value: 1
            },
        ]
    }),
    mutations: {
        setReels(state, reels) {
            state.all = reels
        }
    },
    actions: {
        async getAllReels({ state, commit }) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.get(`${this.state.baseURL}/api/reels`, config);
            if (res.status != 200) return false;
            commit('setReels', res.data);
            return res.data;
        },
        async addReel({ state, commit, dispatch }, reel) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/reels`, reel, config);
            if (res.status != 200 && res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editReel({ state, commit, dispatch }, reel) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/reels/${reel.reelId}`, reel.formReel, config);
            if (res.status != 200) return false;
            const idx = state.all.findIndex(r => r._id === reel.reelId);
            state.all.splice(idx, 1, res.data);
            return res;
        },
        async deleteReel({ state, commit, dispatch }, reel) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/reels/${reel._id}`, config);
            if (res.status != 200) return false;
            const idx = state.all.findIndex(r => r._id === reel._id);
            state.all.splice(idx, 1);
            return res;
        },
        async updatePosition({ state, commit, dispatch}, reels) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/reels/order`,reels ,config);
            if (res.status != 200) commit('setReels', res.data);
            /*commit('setReels', res.data);*/
            return res;
        }

    },
    getters: {
        getReels(state) {
            return state.all;
        },
    },
}

export default reels;