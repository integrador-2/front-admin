import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const paymentMethods = {
    state: () => ({
        all: []
    }),
    mutations: {
        setPaymentMethods(state, paymentMethods) {
            state.all = paymentMethods
        }
    },
    actions: {
        async getAllPaymentMethods({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/payment-methods`, config);
                if (res.status != 200) return false;
                commit('setPaymentMethods', res.data);
                return true;
            } catch (error) {
                return false;
            }
        },
        async addPaymentMethod({ commit, state }, paymentMethods) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/payment-methods`, paymentMethods, config);
            if (res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editPaymentMethod({ commit, state }, paymentMethods) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/payment-methods/${paymentMethods._id}`, paymentMethods.body, config);
            if (res.status != 200) return res;
            const idx = state.all.findIndex(c => c._id === paymentMethods._id);
            state.all.splice(idx, 1, res.data)
            return res;
        },
        async deletePaymentMethod({ commit, state }, paymentMethods) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/payment-methods/${paymentMethods._id}`, config);
            if (res.status != 204) return false;
            const idx = state.all.findIndex(c => c._id === paymentMethods._id);
            state.all.splice(idx, 1)
            return true;
        },
    },
}

export default paymentMethods;