import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const stores = {
    state: () => ({
        stores: [],
    }),
    mutations: {
        setStores(state, stores) {
            state.stores = stores
        }
    },
    actions: {
        async getStoresByStatus({ state, commit }, status) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.get(`${this.state.baseURL}/api/requests/${status.name}?page=${status.page}`, config);
            if (res.status != 200) return false;
            commit('setStores', res.data);
            return res.data;
        },
        async editStoreStatus({ state, commit, dispatch }, statusData) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/requests/${statusData.statudId}`, statusData.body, config);
            if (res.status != 200) return res;
            return res;
        }

    },
}

export default stores;