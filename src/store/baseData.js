import Vue from 'vue'
import Vuex from 'vuex'
import logoColor from '@/assets/img/logo-mercado-lima-color.png'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    logoColor: logoColor,
    hashtag: '#MercadoDeLima',
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
