import configHeaderToken from "../libs/configHeaderToken";
import axios from "axios";
import Vuex from "vuex";
import Vue from "vue";

axios.interceptors.response.use(
    function(response) {
        return response;
    },
    function(error) {
        return error.response || error;
    }
);

//Modules
import baseData from "./baseData";
import status from "./status";
import storesModule from './stores';
import reelsModule from './reels';
import categoriesModule from './categories';
import subCategoriesModule from './subcategories';
import departmentsModule from './departments';
import requestsModule from './requests';
import featuredEntrepreneursModule from './featuredEntrepreneurs'
import metrics from './metrics'
import paymentMethods from './paymentMethods'
import popUpsModule from './popUps'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        baseURL: "https://prueba2.castillodetalentos.edu.pe",
        domain: '',
        authToken: localStorage.getItem("token"),
        iat: localStorage.getItem("iat"),
        navUser: [],
        user: null,
        menu: true,
    },
    mutations: {
        setCurrentUser(state, user) {
            state.user = user;
        },
        setAuthToken(state, token) {
            state.authToken = token;
        },
        setIatToken(state, iat) {
            state.iat = iat;
        },
        setMenuState(state, bool) {
            state.menu = bool;
        },
        setNavUser(state, list) {
            state.navUser = list;
        },
        setMenu(state, menu) {
            state.menu = menu;
        },
    },
    getters: {
        getMenu(state) {
            return state.menu;
        },
    },
    actions: {
        async login({ state, commit, dispatch }, authPayload) {
            const res = await axios.post(
                `${state.baseURL}/api/auth/signin`,
                authPayload
            );
            if (res.status != 200) return res;
            dispatch("replaceTokenAndIat", res.data);
            const config = configHeaderToken(res.data.access_token);
            const { user } = (
                await axios.get(`${state.baseURL}/api/auth/profile`, config)
            ).data;
            commit("setCurrentUser", user);
            commit("setNavUser", user.navigation);
            return res;
        },
        async refreshToken({ commit, state, dispatch }) {
            const config = configHeaderToken(state.authToken);
            const res = await axios.post(
                `${state.baseURL}/api/auth/refresh_token`, {},
                config
            );
            if (res.status != 200) return false;
            dispatch("replaceTokenAndIat", res.data);
            return true;
        },
        replaceTokenAndIat({ state, commit }, data) {
            commit("setAuthToken", data.access_token);
            commit("setIatToken", data.expired_in);
            localStorage.setItem("token", data.access_token);
            localStorage.setItem("iat", data.expired_in);
        },
        logout({ commit, state }) {
            commit("setCurrentUser", null);
            commit("setAuthToken", null);
            commit("setIatToken", null);
            localStorage.removeItem("token");
            localStorage.removeItem("iat");
            localStorage.removeItem("optc");
            localStorage.removeItem("test");
            return null;
        },
    },
    modules: {
        baseData: baseData,
        status: status,
        stores: storesModule,
        reels: reelsModule,
        categories: categoriesModule,
        subCategories: subCategoriesModule,
        departments: departmentsModule,
        requests: requestsModule,
        featuredEntrepreneurs: featuredEntrepreneursModule,
        metrics: metrics,
        paymentMethods: paymentMethods,
        popUps: popUpsModule
    },
});