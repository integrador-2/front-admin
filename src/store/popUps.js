import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const popUp = {
    state: () => ({
        all: []
    }),
    mutations: {
        setPopUp(state, popUp) {
            state.all = popUp
        }
    },
    actions: {
        async getAllPopUps({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/pop_ups`, config);
                if (res.status != 200) return false;
                commit('setPopUp', res.data);
                return true;
            } catch (error) {
                return false;
            }
        },
        async addPopUp({ commit, state }, popUp) {
            try {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/pop_ups`, popUp, config);
            if (res.status != 201) return res;
            state.all.push(res.data);
            return res;
            } catch (error) {
                return res;
            }
        },
        async editPopUp({ commit, state }, popUp) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/pop_ups/${popUp._id}`, popUp.body, config);
            if (res.status != 200) return res;
            const idx = state.all.findIndex(c => c._id === popUp._id);
            state.all.splice(idx, 1, res.data)
            return res;
        },
        async deletePopUp({ commit, state }, popUp) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/pop_ups/${popUp._id}`, config);
            if (res.status != 204) return false;
            const idx = state.all.findIndex(c => c._id === popUp._id);
            state.all.splice(idx, 1)
            return true;
        },
    },
}

export default popUp;