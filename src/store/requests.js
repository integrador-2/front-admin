import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const requests = {
    state: () => ({
        stores: [],
    }),
    mutations: {
        setStoreRequests(state, stores) {
            state.stores = stores
        }
    },
    actions: {
        async getStoreRequestsByStatus({ state, commit }, status) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.get(`${this.state.baseURL}/api/requests/stores/${status.name}?page=${status.page}&limit=${status.limit}&date=${status.date}`, config);
            if (res.status != 200) return false;
            commit('setStoreRequests', res.data);
            return res.data;
        },
        async editStoreRequest({ state, commit, dispatch }, statusData) {
            const config = configHeaderToken(this.state.authToken);
            let res = null;
            if (statusData.type === 'actualizar') {
                res = await axios.put(`${this.state.baseURL}/api/requests/stores/update/${statusData.statudId}`, statusData.body, config);
            } else {
                res = await axios.put(`${this.state.baseURL}/api/requests/stores/create/${statusData.statudId}`, statusData.body, config);
            }
            if (res.status != 200) return res;
            return res;
        }

    },
}

export default requests;