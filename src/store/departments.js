import configHeaderToken from '../libs/configHeaderToken';
import axios from 'axios';

const departments = {
    state: () => ({
        all: []
    }),
    mutations: {
        setDepartments(state, departments) {
            state.all = departments
        }
    },
    actions: {
        async getDepartments({ commit, state }) {
            try {
                const config = configHeaderToken(this.state.authToken);
                const res = await axios.get(`${this.state.baseURL}/api/departments`, config);
                if (res.status != 200) return;
                commit('setDepartments', res.data);
                return;
            } catch (error) {
                return false;
            }
        },
        async addDepartment({ commit, state }, department) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.post(`${this.state.baseURL}/api/departments`, department, config);
            if (res.status != 200 && res.status != 201) return res;
            state.all.push(res.data);
            return res;
        },
        async editDepartment({ commit, state }, department) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.put(`${this.state.baseURL}/api/departments/${department._id}`, department, config);
            if (res.status != 200 && res.status != 201) return res;;
            const idx = state.all.findIndex(d => d._id === department._id);
            state.all.splice(idx, 1, res.data);
            return res;
        },
        async deleteDepartment({ commit, state }, department) {
            const config = configHeaderToken(this.state.authToken);
            const res = await axios.delete(`${this.state.baseURL}/api/departments/${department._id}`, config);
            if (res.status != 200 && res.status != 201) return;
            const idx = state.all.findIndex(s => s._id === department._id);
            state.all.splice(idx, 1);
            return res;
        },
    },
}

export default departments;