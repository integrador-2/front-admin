import Home from "../views/Home";
export default {
    path: "/",
    component: Home,
    children: [{
            path: "/",
            name: "dashboard-page",
            component: () =>
                import ("../views/dashboard/Dashboard.vue"),
        },
        {
            path: "/dashboard/visits",
            name: "dashboard-visitas",
            component: () =>
                import ("../views/dashboard/Visits.vue"),
        },
        {
            path: "/dashboard/request",
            name: "dashboard-request",
            component: () =>
                import ("../views/dashboard/Request.vue"),
        },
        {
            path: "/dashboard/entrepreneur",
            name: "dashboard-entrepreneur",
            component: () =>
                import ("../views/dashboard/Entrepreneur.vue"),
        },
        {
            path: "/dashboard/contacts",
            name: "dashboard-contacts",
            component: () =>
                import ("../views/dashboard/Contacts.vue"),
        },
        {
            path: "/reel",
            name: "reel-crud-page",
        },
        {
            path: '/P404',
            name: 'page-error',
            component: () =>
                import ("../views/Error.vue"),
        },
        {
            path: "/solicitudes",
            name: "request-crud-page",
            component: () =>
                import ("../views/requests/Request.vue"),
        },
        {
            path: "/reels",
            name: "reels-crud-page",
            component: () =>
                import ("../views/reels/Reels.vue"),
        },
        {
            path: "/status",
            name: "status-crud-page",
            component: () =>
                import ("../views/status/Status.vue"),
        },
        {
            path: "/categories",
            name: "categories-crud-page",
            component: () =>
                import ("../views/categories/Categories.vue"),
        },
        {
            path: "/subcategories",
            name: "subcategories-crud-page",
            component: () =>
                import ("../views/subcategories/Subcategories.vue"),
        },
        {
            path: "/departments",
            name: "departments-crud-page",
            component: () =>
                import ("../views/departments/Departments.vue"),
        },
        {
            path: "/provinces",
            name: "provinces-crud-page",
            component: () =>
                import ("../views/provinces/Provinces.vue"),
        },
        {
            path: "/districts",
            name: "districts-crud-page",
            component: () =>
                import ("../views/districts/Districts.vue"),
        },
        {
            path: "/featured-entrepreneurs",
            name: "featured-entrepreneurs-crud-page",
            component: () =>
                import ("../views/featured_entrepreneurs/FeaturedEntrepreneurs.vue"),
        },
        {
            path: "/featured-categories",
            name: "featured-categories-crud-page",
            component: () =>
                import ("../views/featured_categories/FeaturedCategories.vue"),
        },
        {
            path: "/productos",
            name: "products-page",
            component: () =>
                import ("../views/products/Products.vue"),
        },
        {
            path: "/emprendimientos",
            name: "ventures-page",
            component: () =>
                import ("../views/business/Ventures.vue"),
        },
        {
            path: "/emprendimientos/:business_sku",
            name: "entrepreneurship-page",
            component: () =>
                import ("../views/business/Entrepreneurship.vue"),
        },
        {
            path: "/emprendimientos/:business_sku/:product_sku",
            name: "entrepreneurship-product-page",
            component: () =>
                import ("../views/business/Product.vue"),
        },
        {
            path: "/conglomerados",
            name: "emporiums-page",
            component: () =>
                import ("../views/emporiums/Emporiums.vue"),
        },
        {
            path: "/conglomerados/:emporium_sku",
            name: "emporium-page",
            component: () =>
                import ("../views/emporiums/Emporium.vue"),
        },
        {
            path: "/metrics",
            name: "metrics",
            component: () =>
                import ("../views/metrics/Metrics.vue"),
        },
        {
            path: "/payment-methods",
            name: "payment-methods",
            component: () =>
                import ("../views/payment_methods/PaymentMethods.vue"),
        },
        {
            path: "/promociones",
            name: "promociones",
            component: () =>
                import ("../views/campanias/Campanias.vue"),
        },
        {
            path: "/promociones/:campaign_sku",
            name: "promocion-page",
            component: () =>
                import ("../views/campanias/Campania.vue"),
        },
        {
            path: "/promociones/:campaign_sku/dashboard",
            name: "promocion-page-dashboard",
            component: () =>
                import ("../views/campanias/CampaniaDashboard.vue"),
        },
        {
            path: "/pop_ups",
            name: "pop-ups",
            component: () =>
                import ("../views/pop_ups/PopUps.vue"),
        },
        {
            path: "/allies",
            name: "allies-page",
            component: () =>
                import ("../views/allies/Allies.vue"),
        },
    ],
    meta: {
        autenticate: true,
    },
};