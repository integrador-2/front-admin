import tokenMiddleware from '../middlewares/tokenMiddleware';
import Vue from "vue";
import VueRouter from "vue-router";


//routes
import authRoutes from './authRoutes';
import userRoutes from './userRoutes';

Vue.use(VueRouter);

const routes = [
    authRoutes,
    userRoutes
]

const router = new VueRouter({
    mode: "history",
    base: "/mdp-mml-admin",
    routes
});

router.beforeEach(tokenMiddleware);

export default router;