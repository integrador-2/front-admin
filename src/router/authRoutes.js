import Login from '../views/auth/Login';

export default {
    path: '/auth/login',
    name: 'Login-Mercado-Lima',
    component: Login,
}