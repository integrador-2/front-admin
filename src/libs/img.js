import store from "../store/index";

export const formatIMG = (file) => {
    if (!file) return "";
    if (Object.keys(file).length == 0) return "";
    if (!file.filename) return "";
    return store.state.baseURL + "/file/" + file.filename;
}