const OPT_FORMAT = { weekday: "long", year: "numeric", month: "long", day: "numeric" };
const FORMATTER = new Intl.DateTimeFormat("es-PE", OPT_FORMAT);
const SIMPLE_FORMAT = new Intl.DateTimeFormat("es-PE");


export const formatLongDate = (datetime) => {
    if (!datetime) return "";
    if (!Date.parse(datetime)) return "";
    const [YEAR, MONTH, DAY] = datetime.toString().split("-");
    const DATE = new Date(YEAR, MONTH - 1, DAY.substr(0, 2));
    return FORMATTER.format(DATE);
}

export const formatISODate = (datetime) => {
    if (!datetime) return "";
    if (!Date.parse(datetime)) return "";
    const [YEAR, MONTH, DAY] = datetime.toString().split("-");
    const DATE = new Date(YEAR, MONTH - 1, DAY.substr(0, 2));
    return SIMPLE_FORMAT.format(DATE);
}