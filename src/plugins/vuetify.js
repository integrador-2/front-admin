import Vue from "vue";
import Vuetify from "vuetify/lib";
import { es } from 'vuetify/lib/locale';

Vue.use(Vuetify)

export default new Vuetify({
    lang: {
        locales: { es },
        current: 'es'
    },
    theme: {
        themes: {
            light: {
                primary: '#112240',
                primarydark: '#007a74',
                secondary: '#940533',
                tertiary: '#1A4A84',
                blueColor: '#112240',
                danger: '#af0000'
            },
            dark: {
                primary: '#112240',
                primarydark: '#007a74',
                secondary: '#940533',
                tertiary: '#1A4A84',
                blueColor: '#112240',
                danger: '#af0000'
            }
        }

    }
});